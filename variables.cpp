#include <iostream>

using namespace std;

int main()
{
    auto n = 9;
    auto name = std::string{"Tom"};
    int m = 0;

    std::cout << "Hello\n";
    std::cout << "World";
    std::cout << m << '\n';

    if (1)
    { // BEGIN A NEW LIST OF VARIABLES
        std::cout << ("THIS IS TRUE!");
        std::cout << (name);
    } // DISCARD YOUR LIST OF VARIABLES
    else {
        std::cout << "This is else";
    }
    if (0)
    {
        std::cout << "THIS IS FALSE :(";
    }
}