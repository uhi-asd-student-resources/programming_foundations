// Function Declaration / definition
// keyword : function
// name : say_hello
// arguments : none
// body : { stuff in here }
// return value : none
function say_hello() {
    for (let ii = 0; ii < 10; ii++) {
        console.log("Hello world")
    }
    // return undefined
}

// Function call
say_hello()

function calculate_total(a, b) {
    return a + b
}

console.log(calculate_total(2, 3))
console.log(calculate_total(4, 5))
console.log(calculate_total(10, 12))

function fibonacci(n) {
    if (n == 1) { return 1; }
    if (n == 2) { return 1; }
    return fibonacci(n - 1) + fibonacci(n - 2)
}

//for(let ii=0; ii < 10000; ii++ ) {
//    console.log(fibonacci(0))
//}

// 9007199254740991
// let val = Number.max_safe_integer;
// console.log(fibonacci(val + 1));

let my_name = function () {
    return "Bugs Bunny";
}

console.log(my_name());

let my_duck = () => {
    return "Daffy Duck";
}

console.log(my_duck());

// let my_pig = () => "Porky Pig";

// console.log(my_pig());

// let age = my_pig;
// console.log(age);
// let fish = age;
// console.log(fish);
// my_pig = 8
// console.log(my_pig);
// console.log(fish);

// let throwHello = function (resolve) {
//     resolve({ then: function () { throw 'hello'; } });
// }
// let fulfilled = function () {
//     testFailed('fulfilled');
//     finishJSTest();
// }
// let rejected = function (localResult) {
//     testPassed('rejected');
//     result = localResult
//     shouldBeEqualToString('result', 'hello');
//     finishJSTest();
// }

// let p = new Promise(throwHello).then(fulfilled, rejected);


let my_pig = () => "Porky Pig";

console.log(my_pig());


let characters = {
    "hero" : () => my_pig(),
    "hero2" : function() {
        return my_pig();
    }
}

console.log(characters["hero"]())


var add = (function () {
    var counter = 0;
    console.log("counter inside our parent clause: "+counter)
    return function () {
        console.log("inside our inner function")
        counter += 1; return counter
    }
}); // ()
console.log(add)            // function add
let result_from_add = add();
console.log(result_from_add);

console.log(result_from_add()); // 1
console.log(result_from_add()); // 2
console.log(result_from_add()); // 3