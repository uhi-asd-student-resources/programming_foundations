#include <iostream>

int calculate_total(int a,int b);

void say_hello() {
    for(auto ii=0; ii < 10; ii++ ) {
        std::cout << "Hello World\n";
    }
}

void print_string() {
    std::cout << "Bugs Bunny";
}

int main() 
{
    //say_hello();
    //std::cout << calculate_total(2,3) << '\n';

    char val = 127;
    std::cout << std::hex << (char) (val + 1) << '\n';

    // our first anonymous function
    auto mouseButtonHandler = []() { std::cout << "Bugs Bunny\n"; };
    std::cout << &mouseButtonHandler << std::endl;
    // assign our anonymous function
    // we would secondCopy to point to the same place
    auto secondCopy = mouseButtonHandler;
    std::cout << &secondCopy << std::endl;
}

int calculate_total(int a,int b) {
    return a + b;
}

