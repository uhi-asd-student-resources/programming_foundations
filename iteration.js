
// // do somthing if ( condition ) {}
// var count = 0;
// while ( count < 0 ) {
//     console.log("Hello Class");
//     count ++;
// }

// console.log("Yeah, class has ended!")

// count = 0
// do {
//     console.log("Hello Class");
//     count ++;
// } while ( count < 0 )

// console.log("Yeah, class has ended!")

// set jj = 0
// if jj < 10 do something
// add 1 to jj
for (let jj=0; jj < 10; jj++ ) {
    if ( jj == 5 ) break;
    console.log("Yay, I am "+jj+" years old.")
}

var dogs = [ "Mary", "Bonzo", "Woofles", "Rex", "Sooty", "Blacky" ]




for ( let dog in dogs ) {
    console.log(dog)
}

// while loop equivalent
dog_counter = 0
var dog = dogs[dog_counter]
while( dog_counter < dogs.length ) {
    console.log(dog)
    dog_counter ++;
    dog = dogs[dog_counter]
}

