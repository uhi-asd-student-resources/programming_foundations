
# do if ( condition ) {}
count = 0
while True:
    print("Hello Class")
    count += 1 # count = count + 1
    if count == 5:
        break
    
print("Yeah, class has ended!")

# let jj=0; jj < 10; jj++:
for jj in range(0,10):
    if jj == 5:
        break
    print("Yay, I am {} years {}.".format(jj, "old"))

dogs = [ "Mary", "Bonzo", "Woofles", "Rex", "Sooty", "Blacky" ]

for dog in dogs:
    print(dog)

# while loop equivalent
dog_counter = 0
dog = dogs[dog_counter]
while dog_counter < dogs.length:
    print(dog)
    dog_counter += 1
    dog = dogs[dog_counter]