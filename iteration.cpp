#include <iostream>
#include <vector>

int main()
{
    // do if ( condition ) {}
    auto count = 0;
    while (true)
    {
        std::cout << "Hello Class\n";
        count++;
        if (count == 5)
        {
            break;
        }
    }

    std::cout << "Yeah, class has ended!\n";

    count = 0;
    do
    {
        std::cout << "Hello Class";
        count++;
    } while (count < 0);

    for (auto jj = 10; jj > 0; jj--)
    {
        if (jj == 5)
            break;
        std::cout << "Yay, I am " << jj << " years old.\n";
    }

    std::vector<std::string> dogs { "Mary", "Bonzo", "Woofles", "Rex", "Sooty", "Blacky" };

    for (auto dog : dogs)
    {
        std::cout << dog << '\n';
    }
}